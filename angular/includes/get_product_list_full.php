<?php 

	require_once('config.inc.php');
	session_start();

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

    //$company_id = $_GET["company_id"];
    //$type = $_GET["type"];

	if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){
		$sql = "SELECT product.*,ddl_list.ddl_name as product_type_name ,company.company_name FROM product
		LEFT OUTER JOIN ddl_list 
		    on product.category = ddl_list.ddl_id AND ddl_list.ddl_group = 'type_product'
		LEFT OUTER JOIN company
		    on product.company_id = company.company_id
		    ";
	}
	else{
	 	echo "no_permission";
	 	exit();
	}

	require_once('config_to_thai.inc.php');
	$result = $conn->query($sql);
	$arr = array();
	$i = 0;
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 

	$conn->close();




?>