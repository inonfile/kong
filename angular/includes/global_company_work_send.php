<?php 

	require_once('config.inc.php');
	session_start();

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	require_once('config_amount_type.inc.php');
	//require_once('config_type_table_name.inc.php');

	if( ($_SESSION['user_type'] == "3")){
		$sql = '';
		for($i = 0;$i < $amount_type;$i = $i+1){
		$type = $i+1;
		include('config_type_table_name.inc.php');
		include('config_type_table_name_display.inc.php');
		$sql = 	$sql."SELECT $type as type ,'$type_name' as type_name ,ddl_type_product.ddl_name as product_type_name ,".$type_name."_product.product_name,'$type_name_display' as type_name_display ,".$type_name."_po_queue_send.*";
		//if($type!=3)$sql = $sql.", '' as amount_book";
		$sql =$sql." FROM ".$type_name."_po_queue_send
				LEFT OUTER JOIN ".$type_name."_product
			    	on ".$type_name."_po_queue_send.product_id = ".$type_name."_product.product_id 
				LEFT OUTER JOIN ddl_type_product 
				    on ".$type_name."_po_queue_send.product_type = ddl_type_product.id AND ddl_type_product.id_type = ".$type."
				WHERE ".$type_name."_po_queue_send.company_id = ".$_SESSION['company_id'];
		if($i <  $amount_type -1)$sql = $sql." UNION ";
		}
	}
	else{
	 	echo "no_permission";
	 	exit();
	}
//echo $sql;
	require_once('config_to_thai.inc.php');
		
	$result = $conn->query($sql);
	$arr = array();
	$i = 0;
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 

	$conn->close();

?>