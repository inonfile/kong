<?php 

	require_once('config.inc.php');
	require_once('functions.inc.php');

	session_start();

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

    $line = $_GET["line"];
    $state = $_GET["state"];

	if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){

			$sql =  
			get_query_line_by_type(1,'offset',$line,$state)." UNION ".
			get_query_line_by_type(2,'corrugated',$line,$state)." UNION ".
			get_query_line_by_type(3,'bill',$line,$state);
			
			//echo $sql;
	}
	else{
	 	echo "no_permission";
	 	exit();
	}

	require_once('config_to_thai.inc.php');
		
	$result = $conn->query($sql);
	$arr = array();
	$i = 0;
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 

	$conn->close();

?>