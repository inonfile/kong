<?php 

	require_once('config.inc.php');

	session_start();

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	if( $_SESSION['logged_in']==true){
		$sql = "SELECT id,CONCAT_WS(' ', list_worker.firstname , list_worker.lastname) as user_name  FROM list_worker WHERE 1=1 AND is_active=1";
	}
	else{
	 	echo "no_permission";
	 	exit();
	}

	require_once('config_to_thai.inc.php');

	$result = $conn->query($sql);
	$arr = array();
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 
	
	$conn->close();

?>