<?php
// Include required MySQL configuration file and functions
require_once('config.inc.php');

	session_start();
	if( !($_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2"))){
	 	echo "no_permission";
	 	exit();
	}
	
 $mysqli = @new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

 if (mysqli_connect_errno()) {
	 printf("Unable to connect to database: %s", mysqli_connect_error());
	 exit();
 }
	$id_split = $mysqli->real_escape_string($_POST['id_split']);
	$work_id = $mysqli->real_escape_string($_POST['work_id']);
	$line = $mysqli->real_escape_string($_POST['line']);
	$state = $mysqli->real_escape_string($_POST['state']);
	$type = $mysqli->real_escape_string($_POST['type']);


	require_once('config_type_table_name.inc.php');


	if(mysqli_num_rows($mysqli->query(
		"SELECT ".$type_name."_split_flow.id_po_queue FROM ".$type_name."_split_flow 
		LEFT OUTER JOIN ".$type_name."_po_queue
					on ".$type_name."_split_flow.id_po_queue = ".$type_name."_po_queue.id 
		WHERE id_po_queue = $work_id"
	)) == 0){
		echo 'no row';
		exit();
	};

	$response = '';

	$sql = "SELECT * FROM ".$type_name."_po_queue WHERE id =  $work_id";

	$result = $mysqli->query($sql);


	if ($result->num_rows > 0) {

	    while($row = $result->fetch_assoc()) {

			$sql = "INSERT INTO ".$type_name."_product_type_line_log (id_po_queue, po_id, po_recieve_date,recieve_date,amount,state,line,id_machine,id_submit) 
				SELECT 
					".$type_name."_po_queue.id, 
					".$type_name."_po_queue.po_id, 
					".$type_name."_po_queue.recieve_date,
					NOW() ,
					".$type_name."_split_flow.amount,
					'".$state."',
					'".$line."',
					".$type_name."_split_flow.id_machine,
					'". $_SESSION['user_id']."'
				
				FROM ".$type_name."_split_flow
				LEFT OUTER JOIN ".$type_name."_po_queue
					on ".$type_name."_split_flow.id_po_queue = ".$type_name."_po_queue.id
				WHERE ".$type_name."_po_queue.id =  $work_id"; 		
	
				if (!mysqli_query($mysqli, $sql)) {
						$response .= $sql;
					}
					else{
						if(mysqli_query($mysqli, "DELETE FROM ".$type_name."_split_flow WHERE id =  $id_split")){
							$response .= $work_id.",";
						}
					}
			}

	}else {
		$response .= $work_id.'->notfound,';
	}


echo rtrim($response, ",");


?>