<?php 

	require_once('config.inc.php');
	session_start();

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 


	if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){
			$sql = 	"SELECT list_machine.*,list_machine_worker.id as id_work,list_machine_worker.id_worker,list_machine_worker.id_machine,CONCAT_WS(' ', list_worker.firstname , list_worker.lastname) as user_name FROM list_machine 
			LEFT OUTER JOIN list_machine_worker
			on	list_machine.id = list_machine_worker.id_machine
			LEFT OUTER JOIN list_worker
			on	list_machine_worker.id_worker = list_worker.id
			WHERE list_machine.id_type = 3 AND list_machine.id_work = 4";
	}
	else{
	 	echo "no_permission";
	 	exit();
	}

	require_once('config_to_thai.inc.php');
		
	$result = $conn->query($sql);
	$arr = array();
	$i = 0;
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 

	$conn->close();

?>