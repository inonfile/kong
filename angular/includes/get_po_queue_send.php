<?php 

	require_once('config.inc.php');
	session_start();

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	$time_send = $_GET['time'];

	if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){
		$sql = 	"SELECT offset_po_queue.*,ddl_list.ddl_name as product_type_name ,company.company_name,offset_product.product_name,car.car_name ,offset_product.url_picture as img_url
		FROM offset_po_queue
		LEFT OUTER JOIN company
		    on offset_po_queue.company_id = company.company_id
		LEFT OUTER JOIN offset_product
	    on offset_po_queue.product_id = offset_product.product_id 
		LEFT OUTER JOIN ddl_list 
		    on offset_po_queue.product_type = ddl_list.ddl_id AND ddl_list.ddl_group = 'type_product'
		LEFT OUTER JOIN car 
		    on offset_po_queue.car_id = car.car_id
	    WHERE line_current = '-1' AND state = '1' AND time_send = ".$time_send;
	}
	else{
	 	echo "no_permission";
	 	exit();
	}

	require_once('config_to_thai.inc.php');
		
	$result = $conn->query($sql);
	$arr = array();
	$i = 0;
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 

	$conn->close();

?>