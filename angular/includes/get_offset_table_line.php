<?php
	require_once('config.inc.php');

	session_start();

    $link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE) or die("Could not connect to host.");

    $table = $_GET["table"];
    $line = $_GET["line"];
    $state = $_GET["state"];

    if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){
/*		$query = "SELECT ".$table.".*,offset_po_queue.product_id,offset_product.product_name FROM ".$table." 
		LEFT OUTER JOIN offset_po_queue
		    on ".$table.".id = offset_po_queue.id
		LEFT OUTER JOIN offset_product
		    on offset_po_queue.product_id = offset_product.id
		WHERE line = ".$line." AND state = ".$state;*/

		$query = "SELECT offset_po_queue.*,offset_product.product_name,offset_product.paper_type,offset_product.note,offset_product.paper_ream_size,offset_product.print_size,offset_product.color,offset_product.mache,offset_product.wrap,offset_product.lacker,offset_product.work_per_print,list_machine.machine_name ,CONCAT_WS(' ', list_worker.firstname , list_worker.lastname) as user_name ,offset_product.url_picture as img_url
		FROM offset_po_queue
		LEFT OUTER JOIN offset_product
		    on offset_po_queue.product_id = offset_product.product_id
		LEFT OUTER JOIN list_machine
		    on offset_po_queue.id_machine = list_machine.id
		LEFT OUTER JOIN list_worker
		ON offset_po_queue.id_machine_worker = list_worker.id 
		WHERE line_current = ".$line." AND state = ".$state." ORDER BY order_line";
	}
/*	}else if($_SESSION['logged_in']==true&&$_SESSION['user_type'] == "3"&&$table == "po_queue"){
		$query = "SELECT * FROM ".$table." WHERE company_id = ".$_SESSION['company_id'] ;
	}*/
	else{
	 	echo "no_permission";
	 	exit();
	}


	require_once('config_to_thai.inc.php');

	    $result = mysqli_query($link,$query) or die("Data not found");
	    $arr = array();
		if(mysqli_num_rows($result)>0){
			while($row = mysqli_fetch_assoc($result)){
				//echo $row['ddl_name'];
				array_push($arr,$row);
				}
			//$arr = js_thai_encode($arr);
			echo json_encode($arr,JSON_UNESCAPED_UNICODE);
		}

	mysqli_close($link);
	
	
	?>