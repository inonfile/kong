<?php 

	require_once('config.inc.php');
	session_start();

	$link = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($link->connect_error) {
	    die("Connection failed: " . $link->connect_error);
	} 
	$type = $link->real_escape_string($_GET['type']);

	 require_once('config_type_table_name.inc.php');

	if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){
		$sql = 	"SELECT ".$type_name."_po_queue_send.*,ddl_list.ddl_name as product_type_name ,company.company_name,".$type_name."_product.product_name ,".$type_name."_product.url_picture as img_url
		FROM ".$type_name."_po_queue_send
		LEFT OUTER JOIN company
		    on ".$type_name."_po_queue_send.company_id = company.company_id
		LEFT OUTER JOIN ".$type_name."_product
	    	on ".$type_name."_po_queue_send.product_id = ".$type_name."_product.product_id 
		LEFT OUTER JOIN ddl_list 
		    on ".$type_name."_po_queue_send.product_type = ddl_list.ddl_id AND ddl_list.ddl_group = 'type_product'
	    WHERE ".$type_name."_po_queue_send.id = ".$_GET['id'];
	}
	else{
	 	echo "no_permission";
	 	exit();
	}//echo $sql;

	require_once('config_to_thai.inc.php');
		
	$result = $link->query($sql);
	$arr = array();
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 

	$link->close();

?>