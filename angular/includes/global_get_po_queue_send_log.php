<?php 

	require_once('config.inc.php');
	session_start();

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	$addition = "";

	$type = $_POST['type'];

	require_once('config_type_table_name.inc.php');
	require_once('config_type_table_name_display.inc.php');

	if($_POST['company_name']!="")
		$addition = $addition." AND  company.company_name = '".$_POST['company_name']."'";
		//$company_name = $_POST['company_name'];
	if($_POST['group_product']!="")
		$addition = $addition." AND  product_type = ".$_POST['group_product'];
		//$group_product = $_POST['group_product'];

	$startdate = $_POST['startdate'];
	$enddate = $_POST['enddate'];

	if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){
		if($type != '0'){
			$sql = 	"SELECT ".$type_name."_po_queue_send.*,'".$type_name."' as type_name,'".$type_name_display."' as type_name_display ,company.company_name FROM ".$type_name."_po_queue_send
				LEFT OUTER JOIN company
				    on ".$type_name."_po_queue_send.company_id = company.company_id
				WHERE 1=1 ".$addition." AND recieve_date between '$startdate' AND '$enddate'";
			}
		else{
			$sql = "Select * FROM
					( SELECT 1 as type ,'offset' as type_name ,'offset' as type_name_display ,offset_po_queue_send.*,company.company_name FROM offset_po_queue_send
					LEFT OUTER JOIN company
					on offset_po_queue_send.company_id = company.company_id
					WHERE 1=1 ".$addition." AND recieve_date between '$startdate' AND '$enddate'
					UNION
					SELECT 2 as type,'corrugated' as type_name ,'ลูกฟูก' as type_name_display ,corrugated_po_queue_send.*,company.company_name FROM corrugated_po_queue_send
					LEFT OUTER JOIN company
					on corrugated_po_queue_send.company_id = company.company_id
					WHERE 1=1 ".$addition." AND recieve_date between '$startdate' AND '$enddate'
					UNION
					SELECT 3 as type, 'bill' as type_name ,'bill' as type_name_display ,bill_po_queue_send.*,company.company_name FROM bill_po_queue_send
					LEFT OUTER JOIN company
					on bill_po_queue_send.company_id = company.company_id
					WHERE 1=1 ".$addition." AND recieve_date between '$startdate' AND '$enddate')
					 results order by finish_date DESC";
			
		}

	}
	else{
	 	echo "no_permission";
	 	exit();
	}

	require_once('config_to_thai.inc.php');
		
	$result = $conn->query($sql);
	$arr = array();
	$i = 0;
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 

	$conn->close();

?>