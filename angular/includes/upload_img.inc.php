<?php
// Include required MySQL configuration file and functions
require_once('config.inc.php');
include('functions.inc.php');

	session_start();
	if( !($_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2"))){
	 	echo "no_permission";
	 	exit();
	}

 $mysqli = @new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

 if (mysqli_connect_errno()) {
	 printf("Unable to connect to database: %s", mysqli_connect_error());
	 exit();
 }

	  $filename = $_FILES['file']['name'];
	  $meta = $_POST;
	  $destination = $meta['targetPath'] . $filename;
	  $thumbnail_destination = "..//Upload_thumbnail//" . $filename;
	  move_uploaded_file( $_FILES['file']['tmp_name'] , $destination );

	  resize_crop_image(60, 60, $destination, $thumbnail_destination);
	  //echo $destination;
?>