<?php
// Include required MySQL configuration file and functions
require_once('config.inc.php');


	session_start();
	if( !($_SESSION['logged_in']==true&&$_SESSION['user_type'] == "1")){
	 	echo "no_permission";
	 	exit();
	}

 $link = @new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

 if (mysqli_connect_errno()) {
	 printf("Unable to connect to database: %s", mysqli_connect_error());
	 exit();
 }
 	 require_once('config_to_thai.inc.php');

	 $product_name = $link->real_escape_string($_POST['product_name']);
	 $company_id = $link->real_escape_string($_POST['company_id']);
	 $category = $link->real_escape_string($_POST['category']);
	 $note = $link->real_escape_string($_POST['note']);
	 $paper_type_1 = $link->real_escape_string($_POST['paper_type_1_dialog']);
	 $paper_type_2 = $link->real_escape_string($_POST['paper_type_2_dialog']);
	 $paper_type_3 = $link->real_escape_string($_POST['paper_type_3_dialog']);
	 $color = $link->real_escape_string($_POST['color']);
	 $book_per_wrap = $link->real_escape_string($_POST['book_per_wrap_dialog']);
	 $pack_per_bundle = $link->real_escape_string($_POST['pack_per_bundle_dialog']);
	 $paper_ream_size = $link->real_escape_string($_POST['paper_ream_size_dialog']);
	 $paper_per_ream = $link->real_escape_string($_POST['paper_per_ream_dialog']);
	 $cut_amount = $link->real_escape_string($_POST['cut_amount_dialog']);
	 $work_per_print = $link->real_escape_string($_POST['work_per_print_dialog']);
	 $paper_per_set = $link->real_escape_string($_POST['paper_per_set_dialog']);
	 $set_per_book = $link->real_escape_string($_POST['set_per_book_dialog']);
	 $work_per_bigbook = $link->real_escape_string($_POST['work_per_bigbook_dialog']);
	 $book_per_bundle = $link->real_escape_string($_POST['book_per_bundle_dialog']);
	 $product_line = $link->real_escape_string($_POST['product_line']);
	 $url_picture = $link->real_escape_string($_POST['url_picture']);
	 $id_product = $link->real_escape_string($_POST['id_product']);

	$sql = "UPDATE 
				bill_product 
			SET 
				product_name = '$product_name',
				company_id = '$company_id',
				category = '$category',
				note = '$note',
				paper_type_1 = '$paper_type_1',
				paper_type_2 = '$paper_type_2',
				paper_type_3 = '$paper_type_3',
				color = '$color',
				book_per_wrap = '$book_per_wrap',
				pack_per_bundle = '$pack_per_bundle',
				paper_ream_size = '$paper_ream_size',
				paper_per_ream = '$paper_per_ream',
				cut_amount = '$cut_amount',
				work_per_print = '$work_per_print',
				paper_per_set = '$paper_per_set',
				set_per_book = '$set_per_book',
				work_per_bigbook = '$work_per_bigbook',
				book_per_bundle = '$book_per_bundle',
				product_line = '$product_line',
				url_picture = '$url_picture',
				update_date = NOW(),
				update_by = '".$_SESSION['user_id']."'
			WHERE 
				product_id =  $id_product";
				

		if (mysqli_query($link, $sql)) {

			echo "correct";

		}

?>