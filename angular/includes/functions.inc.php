<?php
/**
* Crucial Functions for Application
*
* @package tpc_tutorials
* @file /includes/functions.inc.php
*/

/**
* Redirects to specified page
*
* @param string $page Page to redirect user to
* @return void
*/
function redirect($page) {
 header('Location: ' . $page);
 exit();
}

/**
* Check login status
*
* @return boolean Login status
*/
function check_login_status() {
 // If $_SESSION['logged_in'] is set, return the status
 if (isset($_SESSION['logged_in'])) {
 return $_SESSION['logged_in'];
 }
 return false;
}


//resize and crop image by center
function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 80){
    $imgsize = getimagesize($source_file);
    $width = $imgsize[0];
    $height = $imgsize[1];
    $mime = $imgsize['mime'];
 
    switch($mime){
        case 'image/gif':
            $image_create = "imagecreatefromgif";
            $image = "imagegif";
            break;
 
        case 'image/png':
            $image_create = "imagecreatefrompng";
            $image = "imagepng";
            $quality = 7;
            break;
 
        case 'image/jpeg':
            $image_create = "imagecreatefromjpeg";
            $image = "imagejpeg";
            $quality = 80;
            break;
 
        default:
            return false;
            break;
    }
     
    $dst_img = imagecreatetruecolor($max_width, $max_height);
    $src_img = $image_create($source_file);
     
    $width_new = $height * $max_width / $max_height;
    $height_new = $width * $max_height / $max_width;
    //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
    if($width_new > $width){
        //cut point by height
        $h_point = (($height - $height_new) / 2);
        //copy image
        imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
    }else{
        //cut point by width
        $w_point = (($width - $width_new) / 2);
        imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
    }
     
    $image($dst_img, $dst_dir, $quality);
 
    if($dst_img)imagedestroy($dst_img);
    if($src_img)imagedestroy($src_img);
}

function get_query_line_by_type($type,$type_name,$line,$state) {

 return "SELECT 
 '".$type_name."' as type_name,
 CONCAT_WS('_',".$type_name."_po_queue.id,".$type.") AS id,
 ".$type_name."_po_queue.product_id,
 ".$type_name."_po_queue.id_machine,
 ".$type_name."_po_queue.id_machine_worker,
 ".$type_name."_po_queue.company_id,
 ".$type_name."_po_queue.product_type,
 ".$type_name."_po_queue.po_id,
 ".$type_name."_po_queue.recieve_date,
 ".$type_name."_po_queue.due_date,
 ".$type_name."_po_queue.amount_current,
 ".$type_name."_po_queue.order_line,
 ".$type_name."_po_queue.state,
 ".$type_name."_po_queue.line_current,
 ".$type_name."_product.product_name,
 ".$type_name."_product.url_picture as img_url,
 company.company_name,
 ddl_type_product.ddl_name as product_type_name ,
 list_machine.machine_name ,
 CONCAT_WS(' ', list_worker.firstname , list_worker.lastname) as user_name ,
 ".$type_name."_product.url_picture as img_url

        FROM ".$type_name."_po_queue
        LEFT OUTER JOIN ".$type_name."_product
            on ".$type_name."_po_queue.product_id = ".$type_name."_product.product_id
        LEFT OUTER JOIN list_machine
            on ".$type_name."_po_queue.id_machine = list_machine.id
        LEFT OUTER JOIN list_worker
            on ".$type_name."_po_queue.id_machine_worker = list_worker.id 
        LEFT OUTER JOIN company
            on ".$type_name."_po_queue.company_id = company.company_id 
        LEFT OUTER JOIN ddl_type_product 
            on ".$type_name."_po_queue.product_type = ddl_type_product.id AND ddl_type_product.id_type = ".$type."
        WHERE line_current = ".$line." AND state = ".$state;
    }

function get_query_line_by_type_send($type,$type_name,$line,$state,$time_send) {

 return "SELECT 
 '".$type_name."' as type_name,
 CONCAT_WS('_',".$type_name."_po_queue.id,".$type.") AS id,
 ".$type_name."_po_queue.product_id,
 ".$type_name."_po_queue.id_machine,
 ".$type_name."_po_queue.id_machine_worker,
 ".$type_name."_po_queue.company_id,
 ".$type_name."_po_queue.send_date,
 ".$type_name."_po_queue.car_id,
 ".$type_name."_po_queue.product_type,
 ".$type_name."_po_queue.po_id,
 ".$type_name."_po_queue.recieve_date,
 ".$type_name."_po_queue.due_date,
 ".$type_name."_po_queue.amount_current,
 ".$type_name."_po_queue.order_line,
 ".$type_name."_po_queue.state,
 ".$type_name."_po_queue.line_current,
 ".$type_name."_product.product_name,
 ".$type_name."_product.url_picture as img_url,
 car.car_name,
 company.company_name,
 ddl_type_product.ddl_name as product_type_name ,
 list_machine.machine_name ,
 CONCAT_WS(' ', list_worker.firstname , list_worker.lastname) as user_name ,
 ".$type_name."_product.url_picture as img_url

        FROM ".$type_name."_po_queue
        LEFT OUTER JOIN ".$type_name."_product
            on ".$type_name."_po_queue.product_id = ".$type_name."_product.product_id
        LEFT OUTER JOIN list_machine
            on ".$type_name."_po_queue.id_machine = list_machine.id
        LEFT OUTER JOIN list_worker
            on ".$type_name."_po_queue.id_machine_worker = list_worker.id 
        LEFT OUTER JOIN company
            on ".$type_name."_po_queue.company_id = company.company_id 
        LEFT OUTER JOIN ddl_type_product 
            on ".$type_name."_po_queue.product_type = ddl_type_product.id AND ddl_type_product.id_type = ".$type."
        LEFT OUTER JOIN car 
            on ".$type_name."_po_queue.car_id = car.car_id
        WHERE line_current = ".$line." AND state = ".$state ." AND time_send = ".$time_send;
    }
?>