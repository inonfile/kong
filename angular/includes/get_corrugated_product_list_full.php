<?php 

	require_once('config.inc.php');
	session_start();

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	 $type = $mysqli->real_escape_string($_GET['type']);
	 require_once('config_type_table_name.inc.php');

    //$company_id = $_GET["company_id"];
    //$type = $_GET["type"];

	if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){
		$sql = "SELECT ".$type_name."_product.*,ddl_type_product.ddl_name as product_type_name ,company.company_name FROM ".$type_name."_product
		LEFT OUTER JOIN ddl_type_product 
		    on ".$type_name."_product.category = ddl_type_product.id
		LEFT OUTER JOIN company
		    on ".$type_name."_product.company_id = company.company_id
		     AND 1=1 ";
		    if(isset($_GET['type_product'])){	$sql = $sql."".$type_name."_product.category = ".$_GET['type_product'];}
		}
	else{
	 	echo "no_permission";
	 	exit();
	}

	require_once('config_to_thai.inc.php');
	$result = $conn->query($sql);
	$arr = array();
	$i = 0;
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 

	$conn->close();




?>