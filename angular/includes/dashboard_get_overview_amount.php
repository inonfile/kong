<?php 

	require_once('config.inc.php');
	session_start();

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

	require_once('config_amount_type.inc.php');
	require_once('config_to_thai.inc.php');

	$sum1 = 0;
	$sum2 = 0;

	if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1")){
		$arr = array();
		for($i = 0; $i < $amount_type;$i++){

			$type = $i+1;
			include('config_type_table_name.inc.php');
			include('config_type_table_name_display.inc.php');

		    $sql = "SELECT COUNT(*) FROM ".$type_name."_po_queue WHERE line_step = '0'";

		    if ($result = mysqli_query($link, $sql)) {
				$row = mysqli_fetch_assoc($result); 
				$sum1 = $row['COUNT(*)'];
			}

		    $sql = "SELECT COUNT(*) FROM ".$type_name."_po_queue WHERE line_step != '0'";

		    if ($result = mysqli_query($link, $sql)) {
				$row = mysqli_fetch_assoc($result); 
				$sum2 = $row['COUNT(*)'];
			}

			 $data = [ 'name' => $type_name_display,'name_sys' => $type_name,'amount_wait' => $sum1 , 'amount_inpro' => $sum2];

			 array_push($arr,$data);
		}
	}
	else{
	 	echo "no_permission";
	 	exit();
	}

	require_once('config_to_thai.inc.php');
		
	echo json_encode($arr,JSON_UNESCAPED_UNICODE);

	$conn->close();

?>