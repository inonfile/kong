<?php 

	require_once('config.inc.php');
	session_start();

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 


	if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){
		if(isset($_GET['machine_type'])&&isset($_GET['machine_work']))
			$sql = 	"SELECT * FROM list_machine WHERE id_type = ".$_GET['machine_type']." AND id_work = ".$_GET['machine_work'];
			//$sql = 	"SELECT * FROM list_machine WHERE id_type = ".$_GET['machine_type']." AND id_work = ".$_GET['machine_work'];
		else
			$sql = 	"SELECT * FROM list_machine";
	}
	else{
	 	echo "no_permission";
	 	exit();
	}

	require_once('config_to_thai.inc.php');
		
	$result = $conn->query($sql);
	$arr = array();
	$i = 0;
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 

	$conn->close();

?>