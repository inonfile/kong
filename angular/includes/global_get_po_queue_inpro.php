<?php 

	require_once('config.inc.php');
	session_start();

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	$type = $conn->real_escape_string($_GET['type']);

	require_once('config_type_table_name.inc.php');

	if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){
		$sql = 	"SELECT ".$type_name."_po_queue.*,ddl_type_product.ddl_name as product_type_name ,company.company_name,".$type_name."_product.product_name,ddl_work.ddl_name ,".$type_name."_product.url_picture as img_url
		FROM ".$type_name."_po_queue
		LEFT OUTER JOIN company
		    on ".$type_name."_po_queue.company_id = company.company_id
		LEFT OUTER JOIN ".$type_name."_product
	    	on ".$type_name."_po_queue.product_id = ".$type_name."_product.product_id 
		LEFT OUTER JOIN ddl_type_product
		    on ".$type_name."_po_queue.product_type = ddl_type_product.id 
		LEFT OUTER JOIN ddl_work 
		    on ".$type_name."_po_queue.line_current = ddl_work.id_group AND ddl_work.id_type = ".$type."
	    WHERE line_step != '0'";
	}
	else{
	 	echo "no_permission";
	 	exit();
	}

	require_once('config_to_thai.inc.php');
		
	$result = $conn->query($sql);
	$arr = array();
	$i = 0;
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 

	$conn->close();

?>