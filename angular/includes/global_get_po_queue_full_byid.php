<?php 

	require_once('config.inc.php');
	session_start();

	$link = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($link->connect_error) {
	    die("Connection failed: " . $link->connect_error);
	} 
	$type = $link->real_escape_string($_GET['type']);

	 require_once('config_type_table_name.inc.php');

	if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){
		$sql = 	"SELECT ".$type_name."_po_queue.*,ddl_type_product.ddl_name as product_type_name ,ddl_work.ddl_name as name_work ,company.company_name,".$type_name."_product.product_name,list_machine.machine_name,CONCAT_WS(' ', user.firstname , user.lastname) as user_name ,".$type_name."_product.url_picture as img_url
		FROM ".$type_name."_po_queue
		LEFT OUTER JOIN company
		    on ".$type_name."_po_queue.company_id = company.company_id
		LEFT OUTER JOIN ".$type_name."_product
	    	on ".$type_name."_po_queue.product_id = ".$type_name."_product.product_id 
		LEFT OUTER JOIN list_machine
		    on ".$type_name."_po_queue.id_machine = list_machine.id
		LEFT OUTER JOIN user
			ON ".$type_name."_po_queue.id_machine_worker = user.id 
		LEFT OUTER JOIN ddl_type_product
		    on ".$type_name."_po_queue.product_type = ddl_type_product.id 
		LEFT OUTER JOIN ddl_work 
		    on ".$type_name."_po_queue.line_current = ddl_work.id_group AND ddl_work.id_type = 1
	    WHERE ".$type_name."_po_queue.id = ".$_GET['id'];
	}
	else{
	 	echo "no_permission";
	 	exit();
	}//echo $sql;

	require_once('config_to_thai.inc.php');
		
	$result = $link->query($sql);
	$arr = array();
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 

	$link->close();

?>