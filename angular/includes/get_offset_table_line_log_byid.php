<?php
	require_once('config.inc.php');

	session_start();

    $link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE) or die("Could not connect to host.");

    $find_id = $_GET['find_id'];

    if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){

		$query = "SELECT offset_product_type_line_log.*,ddl_work.ddl_name as name_work , CONCAT_WS(' ', user.firstname , user.lastname) as user_name 
		FROM offset_product_type_line_log
		LEFT OUTER JOIN ddl_work 
		    on offset_product_type_line_log.line = ddl_work.id_group AND ddl_work.id_type = 1
		LEFT OUTER JOIN user
		ON offset_product_type_line_log.id_submit = user.id 
		WHERE 1=1 AND id_po_queue = $find_id ORDER BY submit_date DESC
		";
	}
	else{
	 	echo "no_permission";
	 	exit();
	}

	require_once('config_to_thai.inc.php');

	$result = $link->query($query);
	$arr = array();
	$i = 0;
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 


	
	
	?>