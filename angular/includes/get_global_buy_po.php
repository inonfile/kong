<?php 

	require_once('config.inc.php');
	session_start();

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1")){
		$sql = 	"SELECT global_buy_po.*  ,t.* FROM global_buy_po
			LEFT OUTER JOIN (
            SELECT  '1' as type ,product_name,url_picture as img_url ,product_id,paper_ream_size as size ,paper_type as paper_type_1  ,'' as paper_type_2 ,'' as paper_type_3 FROM offset_product
            UNION
            SELECT  '2' as type ,product_name,url_picture as img_url ,product_id,size_paper as size,paper_type as paper_type_1  ,'' as paper_type_2 ,'' as paper_type_3  FROM corrugated_product
            UNION
            SELECT  '3' as type ,product_name,url_picture as img_url ,product_id,paper_ream_size as size,paper_type_1 ,paper_type_2 ,paper_type_3  FROM bill_product
            ) as t
on t.product_id = global_buy_po.id_product AND  t.type = global_buy_po.type 

";
	}
	else{
	 	echo "no_permission";
	 	exit();
	}

	require_once('config_to_thai.inc.php');
		
	$result = $conn->query($sql);
	$arr = array();
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 

	$conn->close();

?>