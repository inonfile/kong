<?php
	require_once('config.inc.php');

	session_start();

    $link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE) or die("Could not connect to host.");

    $line = $_GET["line"];
    $state = $_GET["state"];
    $type = $_GET["type"];

	require_once('config_type_table_name.inc.php');

    if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){
/*		$query = "SELECT ".$table.".*,offset_po_queue.product_id,offset_product.product_name FROM ".$table." 
		LEFT OUTER JOIN offset_po_queue
		    on ".$table.".id = offset_po_queue.id
		LEFT OUTER JOIN offset_product
		    on offset_po_queue.product_id = offset_product.id
		WHERE line = ".$line." AND state = ".$state;*/

		$query = "SELECT ".$type_name."_split_flow.id as id_split,".$type_name."_split_flow.amount as amount_split,".$type_name."_po_queue.*,".$type_name."_product.*,company.company_name,ddl_type_product.ddl_name as product_type_name ,list_machine.machine_name ,CONCAT_WS(' ', list_worker.firstname , list_worker.lastname) as user_name ,".$type_name."_product.url_picture as img_url
		FROM ".$type_name."_po_queue
		RIGHT OUTER JOIN ".$type_name."_split_flow
			on ".$type_name."_po_queue.id = ".$type_name."_split_flow.id_po_queue
		LEFT OUTER JOIN ".$type_name."_product
		    on ".$type_name."_po_queue.product_id = ".$type_name."_product.product_id
		LEFT OUTER JOIN list_machine
		    on ".$type_name."_split_flow.id_machine = list_machine.id
		LEFT OUTER JOIN list_worker
			on ".$type_name."_split_flow.id_machine_worker = list_worker.id 
		LEFT OUTER JOIN company
			on ".$type_name."_po_queue.company_id = company.company_id 
		LEFT OUTER JOIN ddl_type_product 
		    on ".$type_name."_po_queue.product_type = ddl_type_product.id AND ddl_type_product.id_type = ".$type."
		WHERE line = ".$line."
		ORDER BY order_line";
	}
/*	}else if($_SESSION['logged_in']==true&&$_SESSION['user_type'] == "3"&&$table == "po_queue"){
		$query = "SELECT * FROM ".$table." WHERE company_id = ".$_SESSION['company_id'] ;
	}*/
	else{
	 	echo "no_permission";
	 	exit();
	}


	require_once('config_to_thai.inc.php');

	    $result = mysqli_query($link,$query) or die("Data not found");
	    $arr = array();
		if(mysqli_num_rows($result)>0){
			while($row = mysqli_fetch_assoc($result)){
				//echo $row['ddl_name'];
				array_push($arr,$row);
				}
			//$arr = js_thai_encode($arr);
			echo json_encode($arr,JSON_UNESCAPED_UNICODE);
		}

	mysqli_close($link);
	
	
	?>