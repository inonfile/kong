<?php 

	require_once('config.inc.php');
	session_start();

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	$id_po_queue = $conn->real_escape_string($_GET['id_po_queue']);
	$id_machine = $conn->real_escape_string($_GET['id_machine']);

	if( $_SESSION['logged_in']==true&&($_SESSION['user_type'] == "1"||$_SESSION['user_type'] == "2")){
		$sql = 	"SELECT*
		FROM bill_book
	    WHERE id_po_queue = '$id_po_queue' AND id_machine = '$id_machine' 
	    ORDER BY datetime";
	}
	else{
	 	echo "no_permission";
	 	exit();
	}

	require_once('config_to_thai.inc.php');
		
	$result = $conn->query($sql);
	$arr = array();
	if($result != '')
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) 
			array_push($arr,$row);
		echo json_encode($arr,JSON_UNESCAPED_UNICODE);
	} 

	$conn->close();

?>